:<<'info'
shellnotes - defaults.sh
(C) Dimitris Marakomihelakis
Released under the "All rights reserved" category. See the RIGHTS.txt file
in /docs/github/ for its full text.
info

NOTES_EDITOR="$(eval cat ~/.shellnotes/util/shellnotes/sd/sd-input1.txt)"
QUICK_NOTES_EDITOR="$(eval cat ~/.shellnotes/util/shellnotes/sd/sd-input2.txt)"
DEFAULT_PATH="$(eval echo $(cat ~/.shellnotes/util/shellnotes/sd/sd-input3.txt))"

